
InMoment eSaaS Mobile SDK
====================

This repository covers all content that is publicly available for consumption, that is provided by InMoment eSaaS.

## Instructions and documents

- [Overview](Overview.md)

- [Native App Integration - Android](Native_App_Integration_Android.md)

- [Native App Integration - iOS](Native_App_Integration_iOS.md)





## Contacts

* **Janet May**- [jmay@inmoment.com](mailto:jmay@inmoment.com)