#! /bin/bash

# Custom Functions for logging
DIRNAME=`dirname "$0"`
cd "${DIRNAME}"

jazzy --config jazzy.yml

# Copy Images from /Docs/Guides/img to Jazzy/img folder.
cp -R ./Docs/Guides/img ./Jazzy
