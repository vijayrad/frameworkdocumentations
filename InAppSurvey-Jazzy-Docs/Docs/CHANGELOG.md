# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
Upcoming major changes will be listed here.

## [2.7.0] - Sept 2020
### Changed:
- Built module with Xcode 12 and Swift 5.3

## [2.6.0] - May 2020
### Changed:
- Built module with Xcode 11.5 and Swift 5.2

## [2.5.0] - Sept 2019
### Changed:
- Built module with Xcode 11 and Swift 5.1

## [2.4.0] - Aug 2019
### Changed:
- Built module with Xcode 11 and Swift 5.1

## [2.3.0] - May 2019
### Changed:
- Built module with Xcode 10.2 and Swift 5.0

## [2.2.0] - October 2018
### Changed:
- Built module with Xcode 10.1 and Swift 4.2.1

## [2.1.0] - September 2018
### Changed:
- Built module with Xcode 10 and Swift 4.2
- Changed usage metrics to be sent to Mixpanel instead of AWS API Gateway

## [2.0.0] - May 2018
### Changed:
- Survey presentation logic was moved from the `InMoment` class to the `FeedbackManager` class.
- The `SurveyListener` protocol was renamed to `FeedbackManagerDelegate`, and its methods were fundamentally changed and improved.
### Added:
- Analytics and logging events are now sent to InMoment by default. These events should not contain any personally identifiable information (PII) and are used strictly for debugging purposes.
- A new `FeedbackManager` class was added, to abstract feedback logic from future additions to the SDK. `FeedbackManager`'s default constructor was made publicly accessible, so multiple instances may be created.
- Exposed more variables and methods on the `Survey` protocols that will allow app developers greater ability to customize the survey's appearance.
### Removed:
- The `SurveyListener` protocol was removed and replaced with a new construct.
- The `SurveyStyle` struct was removed and replaced with a new construct.
