
InMoment eSaaS Mobile SDK
====================

This repository covers all content that is publicly available for consumption, that is provided by InMoment eSaaS.

## Instructions and documents

- [Overview](overview.html)

- [Native App Integration - Android](native-android-app-integration.html)

- [Native App Integration - iOS](native-ios-app-integration.html)





## Contacts

* **Janet May**- [jmay@inmoment.com](mailto:jmay@inmoment.com)