
# InAppSurvey SDK Documents Creation

This repository is used to create InAppSurvey SDK documentations using Jazzy tool. 

## Instructions

* Update all .md files with internal reference as .html instead of .md (Linking of pages inside pages).
* Double click on **Run.command** script file.
* **Run.command** basically runs `jazzy --config jazzy.yml` and then copy images from `/Docs/Guides/img` to `Jazzy/img` folder.
* If you want to remove search documentation header from webpages then you need to remove search div from all .html files inside Jazzy folder.
