
InMoment eSaaS Mobile SDK
====================

This repository covers all content that is publicly available for consumption, that is provided by InMoment eSaaS.

## Instructions and documents

- [Overview](overview.html)

- [Cordova Plugin Integration](cordova-integration.html)

- [Custom Prompt](customprompt.html)






## Contacts

* **Janet May**- [jmay@inmoment.com](mailto:jmay@inmoment.com)